Present repository deploys provided list of docker images in cvmfs.

Jenkins job that takes care of uploading the content on the repo:

https://lcgapp-services.cern.ch/cvmfs-jenkins/job/unpacked.cern.ch/

Statistics for the storage system based on S3:

https://filer-carbon.cern.ch/grafana/d/CpZ5wupmz/s3-bucket-statistics?orgId=1&var-bucket=cvmfs-unpacked&from=now%2Fd&to=now%2Fd&refresh=30s